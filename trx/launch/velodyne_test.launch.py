#!/usr/bin/env python3

import os
import yaml

from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration, Command
from launch_ros.actions import Node
import ament_index_python.packages
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():
    # Set LOG format
    os.environ['RCUTILS_CONSOLE_OUTPUT_FORMAT'] = '{time} [{name}] [{severity}] {message}'

    velodyne_params = os.path.join(ament_index_python.packages.get_package_share_directory('trx'), 'param', 'velodyne.params.yaml')

    with open(velodyne_params, 'r') as f:
        driver_params = yaml.safe_load(f)['velodyne_driver_node']['ros__parameters']

    convert_share_dir = ament_index_python.packages.get_package_share_directory('velodyne_pointcloud')
    with open(velodyne_params, 'r') as f:
        convert_params = yaml.safe_load(f)['velodyne_convert_node']['ros__parameters']
    convert_params['calibration'] = os.path.join(convert_share_dir, 'params', 'VLP16db.yaml')

    with open(velodyne_params, 'r') as f:
        laserscan_params = yaml.safe_load(f)['velodyne_laserscan_node']['ros__parameters']

    velodyne_container = ComposableNodeContainer(
            name='velodyne_container',
            namespace='',
            package='rclcpp_components',
            executable='component_container',
            composable_node_descriptions=[
                ComposableNode(
                    package='velodyne_driver',
                    plugin='velodyne_driver::VelodyneDriver',
                    name='velodyne_driver_node',
                    parameters=[driver_params]),
                ComposableNode(
                    package='velodyne_pointcloud',
                    plugin='velodyne_pointcloud::Convert',
                    name='velodyne_convert_node',
                    parameters=[convert_params]),
            ],
            output='both',
    )

    # Define LaunchDescription variable
    ld = LaunchDescription()

    ld.add_action(velodyne_container)

    return ld
