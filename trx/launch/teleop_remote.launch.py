#!/usr/bin/env python3

import os

from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

def generate_launch_description():
    joy_node = Node(
            package='joy',
            executable='joy_node',
            output='screen',
            parameters=[{
                'dev': '/dev/input/js0'
            }],
        )

    teleop_params = os.path.join(
                get_package_share_directory('argos'),
                'param', 'teleop_params_remote.yaml'
            )

    teleop_node = Node(
        package='ghost_teleop',
        executable='teleop_node',
        output='screen',
        parameters=[teleop_params],
        remappings=[('/cmd_vel', '/joy_vel')],
    )

    # Define LaunchDescription variable
    ld = LaunchDescription()

    ld.add_action(joy_node)
    ld.add_action(teleop_node)

    return ld
