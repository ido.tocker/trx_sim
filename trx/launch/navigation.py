import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from launch.conditions import IfCondition


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='false')
    rviz_file = DeclareLaunchArgument('rviz_file', default_value='trx_sim.rviz')
    use_rviz_arg = DeclareLaunchArgument('use_rviz', default_value='true')
    map_dir = LaunchConfiguration(
        'map',
        default=os.path.join(
            get_package_share_directory('trx'),
            'map',
            'map.yaml'))
    param_name = LaunchConfiguration(
        'params_file',
        default='nav.params.yaml')

    # nav_commander_params = LaunchConfiguration(
    #     'nav_commander_params',
    #     default=os.path.join(
    #         get_package_share_directory('nav_commander'),
    #         'param',
    #         'nav_commander.params.yaml'))
    # nav_commander_node = Node(
    #     package='nav_commander',
    #     executable='nav_commander_node',
    #     parameters=[nav_commander_params, {'use_sim_time': True}],
    #
    #     output='screen'
    # )
    rviz_dir = os.path.join(get_package_share_directory('trx'), 'rviz')
    rviz = Node(package="rviz2", executable="rviz2",
                arguments=['-d', [rviz_dir, '/', LaunchConfiguration('rviz_file')]],
                parameters=[{'use_sim_time': False}],
                output="screen",
                respawn=True,
                respawn_delay=5,
                condition=IfCondition(
                    LaunchConfiguration('use_rviz'))
                )

    nav2_launch_file_dir = os.path.join(get_package_share_directory('trx'), 'launch')

    return LaunchDescription([

        DeclareLaunchArgument(
            'params_file',
            default_value='nav.params.yaml',
            description='Full path to param file to load'),

        DeclareLaunchArgument(
            'use_sim_time',
            default_value='true',
            description='Use simulation (Gazebo) clock if true'),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([nav2_launch_file_dir, '/nav_bringup.py']),
            launch_arguments={
                'map': map_dir,
                'use_sim_time': use_sim_time,
                'params_file': [get_package_share_directory('trx'), '/param/',
                                LaunchConfiguration('params_file')]}.items(),
        ),
        rviz_file,
        use_rviz_arg,
        # rviz,

        # nav_commander_node,
        # Node(
        #     package='rviz2',
        #     executable='rviz2',
        #     name='rviz2',
        #     arguments=['-d', rviz_config_dir],
        #     parameters=[{'use_sim_time': use_sim_time}],
        #     output='screen'),
    ])
