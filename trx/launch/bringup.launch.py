#!/usr/bin/env python3

import os
import yaml
import launch
import launch.actions
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
from launch.substitutions import LaunchConfiguration
import xacro
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode
from ament_index_python.packages import get_package_share_directory
import ament_index_python.packages

def generate_launch_description():
    use_rviz_arg = DeclareLaunchArgument('use_rviz', default_value='true')
    broadcast_odom_tf = DeclareLaunchArgument('odom_tf', default_value='true')
    use_voice_to_text = DeclareLaunchArgument(
        'voice_to_text', default_value='false')
    use_person_manager = DeclareLaunchArgument(
        'person_manager', default_value='false')
    rviz_file = DeclareLaunchArgument(
        'rviz_file', default_value='trx_sim.rviz')

    remappings = [('/tf', 'tf'),
                  ('/tf_static', 'tf_static')]

    xacro_path = os.path.join(
        get_package_share_directory('trx_description'),
        #'/home/argos/hri_ws/install/trx_description/share/trx_description',
        'urdf',
        'trx.urdf')

    urdf = xacro.process(
        xacro_path,
        mappings={
            'home_dir': os.getenv('HOME')
        }
    )

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='screen',
        parameters=[{'robot_description': urdf }],
        remappings=remappings,
    )

    odom_tf_broadcaster = Node(
        package='autokit_utils',
        executable='odom_tf_broadcaster',
        remappings={('/odom', '/mobile_base_controller/odom')},
        parameters=[{
            'body_frame': 'base_footprint',
        }],
        output='screen',
        condition=launch.conditions.IfCondition(
            LaunchConfiguration('odom_tf'))
    )
    twist_mux_params = os.path.join(get_package_share_directory('trx'),
                                    'param', 'twist_mux.params.yaml')
    
    teleop_params = os.path.join(get_package_share_directory('trx'),
                                    'param', 'teleop_params.yaml')

    twist_mux = Node(
        package='twist_mux',
        executable='twist_mux',
        output='screen',
        # remappings={('/cmd_vel_out', '/trx_base_controller/cmd_vel_unstamped')},
        remappings={('/cmd_vel_out', '/mobile_base_controller/cmd_vel')},
        parameters=[twist_mux_params]
    )

    velodyne_params = os.path.join(ament_index_python.packages.get_package_share_directory('trx'), 'param', 'velodyne.params.yaml')

    with open(velodyne_params, 'r') as f:
        driver_params = yaml.safe_load(f)['velodyne_driver_node']['ros__parameters']

    convert_share_dir = ament_index_python.packages.get_package_share_directory('velodyne_pointcloud')
    with open(velodyne_params, 'r') as f:
        convert_params = yaml.safe_load(f)['velodyne_convert_node']['ros__parameters']
    convert_params['calibration'] = os.path.join(convert_share_dir, 'params', 'VLP16db.yaml')

    with open(velodyne_params, 'r') as f:
        laserscan_params = yaml.safe_load(f)['velodyne_laserscan_node']['ros__parameters']

    velodyne_container = ComposableNodeContainer(
            name='velodyne_container',
            namespace='',
            package='rclcpp_components',
            executable='component_container',
            composable_node_descriptions=[
                ComposableNode(
                    package='velodyne_driver',
                    plugin='velodyne_driver::VelodyneDriver',
                    name='velodyne_driver_node',
                    parameters=[driver_params]),
                ComposableNode(
                    package='velodyne_pointcloud',
                    plugin='velodyne_pointcloud::Convert',
                    name='velodyne_convert_node',
                    parameters=[convert_params]),
            ],
            output='both',
    )

    return launch.LaunchDescription([
        DeclareLaunchArgument('use_joy', default_value='true'),
        DeclareLaunchArgument('use_laserscan', default_value='true'),
        DeclareLaunchArgument('joy_input', default_value='/dev/input/js0'),
        DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'),
        Node(
            package='joy_linux',
            executable='joy_linux_node',
            output='screen',
            parameters=[{
                'dev': '/dev/input/js0'
            }],
            remappings=[],
            condition=launch.conditions.IfCondition(
                LaunchConfiguration('use_joy'))
        ),
        Node(
            package='teleop_twist_joy', executable='teleop_node',
            name='teleop_twist_joy_node', parameters=[teleop_params],
            remappings={
                ('/cmd_vel', '/joy_vel')},
            condition=launch.conditions.IfCondition(
                LaunchConfiguration('use_joy'))
        ),

        Node(
            package='pointcloud_to_laserscan', executable='pointcloud_to_laserscan_node',
            remappings=[('cloud_in', '/velodyne_points'),
                        ('scan', '/velodyne_scan')],
            parameters=[{
                'target_frame': 'velodyne',
                'transform_tolerance': 0.01,
                'min_height': -0.3,
                'max_height': 0.8,
                'angle_min': -3.14159,  # -2*M_PI
                'angle_max': 3.14159,  # 2*M_PI
                'angle_increment': 0.0087,  # M_PI/360.0
                'scan_time': 0.3333,
                'range_min': 0.5,
                'range_max': 50.0,
                'use_inf': True,
                'inf_epsilon': 1.0,
            }],
            condition=launch.conditions.IfCondition(
                LaunchConfiguration('use_laserscan')),
            name='pointcloud_to_laserscan'
        ),
        robot_state_publisher,
        twist_mux,
        broadcast_odom_tf,
        odom_tf_broadcaster,
        velodyne_container
    ])
