#!/usr/bin/env python3

import os
import launch
import launch.actions
from launch.actions import DeclareLaunchArgument
from launch.launch_description_sources.python_launch_description_source import \
    PythonLaunchDescriptionSource
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
from launch.substitutions import LaunchConfiguration, Command
import xacro
from launch.actions import ExecuteProcess, IncludeLaunchDescription, RegisterEventHandler


def generate_launch_description():
    use_rviz_arg = DeclareLaunchArgument('use_rviz', default_value='true')
    broadcast_odom_tf = DeclareLaunchArgument('odom_tf', default_value='true')
    use_voice_to_text = DeclareLaunchArgument('voice_to_text', default_value='false')
    use_person_manager = DeclareLaunchArgument('person_manager', default_value='false')
    rviz_file = DeclareLaunchArgument('rviz_file', default_value='trx_sim.rviz')
    world = LaunchConfiguration(
        'world',
        default='~/ghost_ws/install/aws_robomaker_hospital_world/share/aws_robomaker_hospital_world/worlds/hospital_offset.world')
    X = DeclareLaunchArgument('x', default_value='0.0')
    Y = DeclareLaunchArgument('y', default_value='0.0')
    Z = DeclareLaunchArgument('z', default_value='0.0')
    Yaw = DeclareLaunchArgument('yaw', default_value='0.0')

    remappings = [('/tf', 'tf'),
                  ('/tf_static', 'tf_static')]

    xacro_path = os.path.join(
        get_package_share_directory('trx_description'),
        'urdf',
        'trx_gazebo.xacro')

    urdf = xacro.process(
        xacro_path,
        mappings={
            'home_dir': os.getenv('HOME')
        }
    )

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='screen',
        parameters=[{'use_sim_time': True,
                     'robot_description': urdf
                     }],
        # arguments=[urdf],
        remappings=remappings,
    )

    spawn_entity = Node(package='gazebo_ros', executable='spawn_entity.py',
                        arguments=['-topic', 'robot_description',
                                   '-entity', 'trx',
                                   '-x', LaunchConfiguration('x'),
                                   '-y', LaunchConfiguration('y'),
                                   '-z', LaunchConfiguration('z'),
                                   '-Y', LaunchConfiguration('yaw')
                                   ],
                        parameters=[{'use_sim_time': True}],
                        output='screen')

    twist_scaler = Node(
        package='autokit_utils',
        executable='twist_scaler',
        remappings=[('cmd_vel', '/nav_vel'),
                    ('scaled_cmd_vel', '/scaled_nav_vel')],
        parameters=[{
            'scale_x': 1.0,
            'scale_y': 1.0,
            'scale_yaw': 5.0,
            'max_vel.x': 0.5,
            'min_vel.x': -0.5,
            'max_vel.y': 0.0,
            'min_vel.y': 0.0,
            'max_vel.yaw': 1.0,
            'min_vel.yaw': -1.0,
            'use_sim_time': True
        }],
        name='twist_scaler'
    )

    voice_to_text = Node(
        package='voice_to_text_pub',
        executable='voice_to_text_node',
        output='screen',
        parameters=[],
        condition=launch.conditions.IfCondition(
            LaunchConfiguration('voice_to_text'))
    )

    person_manager = Node(
        package='person_manager',
        executable='person_manager',
        output='screen',
        parameters=[],
        condition=launch.conditions.IfCondition(
            LaunchConfiguration('person_manager'))
    )

    odom_tf_broadcaster = Node(
        package='autokit_utils',
        executable='odom_tf_broadcaster',
        remappings={('/odom', '/p3d_odom')},
        parameters=[{
            'body_frame': 'base_footprint',
            'use_sim_time': True
        }],
        output='screen',
        condition=launch.conditions.IfCondition(
            LaunchConfiguration('odom_tf'))
    )
    twist_mux_params = os.path.join(get_package_share_directory('trx'),
                                    'param', 'twist_mux.params.yaml')

    twist_mux = Node(
        package='twist_mux',
        executable='twist_mux',
        output='screen',
        # remappings={('/cmd_vel_out', '/trx_base_controller/cmd_vel_unstamped')},
        remappings={('/cmd_vel_out', '/cmd_vel')},
        parameters=[twist_mux_params]
    )
    commands_handler = Node(
        package='commands_handler',
        executable='commands_handler_node',
        output='screen',
        parameters=[],
        condition=launch.conditions.IfCondition(
            LaunchConfiguration('voice_to_text'))
    )

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([os.path.join(
            get_package_share_directory('gazebo_ros'), 'launch'), '/gazebo.launch.py']),
        launch_arguments={'gui': 'true',
                          'minimal_comms': 'false',
                          'force_system': 'true',
                          'server_required': 'true',
                          'world': world,
                          'verbose': 'false'
                          }.items()
    )

    return launch.LaunchDescription([
        DeclareLaunchArgument('use_joy', default_value='true'),
        DeclareLaunchArgument('use_laserscan', default_value='true'),
        DeclareLaunchArgument('joy_input', default_value='/dev/input/js0'),
        DeclareLaunchArgument(
            'use_sim_time',
            default_value='true',
            description='Use simulation (Gazebo) clock if true'),
        # Node(
        #     package='joy',
        #     executable='joy_node',
        #     output='screen',
        #     parameters=[{
        #         'dev': '/dev/input/js0'
        #     }],
        #     remappings=[],
        #     condition=launch.conditions.IfCondition(
        #         LaunchConfiguration('use_joy'))
        # ),
        #
        Node(
            package='pointcloud_to_laserscan', executable='pointcloud_to_laserscan_node',
            remappings=[('cloud_in', '/velodyne_points'),
                        ('scan', '/velodyne_scan')],
            parameters=[{
                'target_frame': 'velodyne',
                'transform_tolerance': 0.01,
                'min_height': -0.3,
                'max_height': 0.8,
                'angle_min': -3.14159,  # -2*M_PI
                'angle_max': 3.14159,  # 2*M_PI
                'angle_increment': 0.0087,  # M_PI/360.0
                'scan_time': 0.3333,
                'range_min': 0.5,
                'range_max': 50.0,
                'use_inf': True,
                'inf_epsilon': 1.0,
                'use_sim_time': True
            }],
            condition=launch.conditions.IfCondition(
                LaunchConfiguration('use_laserscan')),
            name='pointcloud_to_laserscan'
        ),
        X, Y, Z, Yaw,
        gazebo,
        robot_state_publisher,
        spawn_entity,
        twist_mux,
        broadcast_odom_tf,
        odom_tf_broadcaster,
        use_voice_to_text,
        commands_handler,
        voice_to_text,
        use_person_manager,
        person_manager,
        # twist_scaler
    ])
