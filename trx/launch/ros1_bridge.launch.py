import os

import launch
import launch_ros.actions

from ament_index_python.packages import get_package_share_directory

def generate_launch_description():


    bridge = launch_ros.actions.Node(
        package='ros1_bridge',
        executable='dynamic_bridge',
        parameters=[],
        remappings=[('/tf','/cancel_tf'),('/tf_static','/cancel_tf_static'),('/joint_states','/cancel_joint_states')],
        output='screen'
        )

    return launch.LaunchDescription([
        bridge,
            ])