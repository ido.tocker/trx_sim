#!/bin/bash
# Store the contents of every tmux pane
#

# Get timestamp
timestamp="$(date "+%d-%m-%Y_%S-%M-%H")"

# Get home dir
tmux_home_directory="$HOME"

# Define output directory
output_directory="${tmux_home_directory}/wg_bags/tmux_logs/$(hostname -s)-${timestamp}"

# Create output directory
mkdir -p ${output_directory}

# Display output directory
echo "Output directory: ${output_directory}"

# Display date
date | tee "${output_directory}/date"

# Iterate over tmux sessions
tmux_sessions="$(tmux list-sessions -F "#{session_id}")"
for tmux_session in $tmux_sessions; do

  tmux_session_name="$(tmux display-message -t "${tmux_session}" -p "#{session_name}")"
  echo "Session: ${tmux_session_name} (${tmux_session})" | tee -a "${output_directory}/overview"

  # Iterate over tmux windows
  tmux_windows="$(tmux list-windows -F "#{window_id}" -t "${tmux_session}")"
  for tmux_window in $tmux_windows; do
    tmux_window_name="$(tmux display-message -t "${tmux_session}:${tmux_window}" -p "#{window_name}")"
    echo "  Window: ${tmux_window_name} (${tmux_window})" | tee -a "${output_directory}/overview"

    # Iterate over tmux panes
    tmux_panes="$(tmux list-panes -F "#{pane_id}" -t "${tmux_session}:${tmux_window}")"
    for tmux_pane in $tmux_panes; do
      tmux_pane_name="$(tmux display-message -c "${tmux_session}:${tmux_window}" -t "${tmux_pane}" -p "#{pane_title}")"
      echo "    Pane: ${tmux_pane_name} (${tmux_pane})" | tee -a "${output_directory}/overview"

      tmux capture-pane -t "${tmux_pane}" -b temp-capture-buffer-${timestamp} -S -
      tmux save-buffer -b temp-capture-buffer-${timestamp} "${output_directory}/${tmux_session_name}-${tmux_window_name}-${tmux_pane_name}.log"
      tmux delete-buffer -b capture-buffer-${timestamp}
    done
  done
done