#!/bin/bash

my_ws=hri_ws

# build specified packages
function cobp() {
    PKGs=$*
    curr_dir=$pwd
    cd ~/${my_ws}
    source ~/.bashrc
    colcon build --symlink-install --packages-select $PKGs --cmake-args -DCMAKE_BUILD_TYPE=Release
    cd $curr_dir
    source ~/.bashrc
}

rm -r ~/$my_ws/install ~/$my_ws/build

# stage 1: build interfaces with old Connext
# source /opt/rti.com/rti_connext_dds-5.3.1/resource/scripts/rtisetenv_x64Linux3gcc5.4.0.bash
# export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
# unset CONNEXTDDS_DIR

cobp grid_map_cmake_helpers nav2_msgs nav_2d_msgs grid_map_msgs hri_interfaces
cobp dwb_msgs # needs to build after nav_2d_msgs

# stage 2: build rmw_connextdds
# source ~/src/rti_connext_dds-6.1.0/resource/scripts/rtisetenv_x64Linux4gcc7.3.0.bash
# export CONNEXTDDS_DIR=${NDDSHOME}

# cobp rti_connext_dds_cmake_module rmw_connextdds rmw_connextdds_common rmw_connextddsmicro
# export RMW_IMPLEMENTATION=rmw_connextdds

# stage 3: build nav2 overlays

cobp nav_2d_utils 
cobp nav2_costmap_2d
cobp nav2_navfn_planner dwb_core dwb_critics dwb_plugins 
cobp nav2_dwb_controller costmap_queue nav2_controller nav2_planner nav2_recoveries autokit_behavior_tree

# # stage 4: build all other packages
(cd ~/$my_ws && colcon build --symlink-install --cmake-args -DCMAKE_BUILD_TYPE=Release) && source ~/.bashrc

# notify-send -t 100 "autokit packages finished building" "make sure there are no errors"
