robot_name='trx'
gazebo_world='~/hri_ws/install/aws_robomaker_hospital_world/share/aws_robomaker_hospital_world/worlds/hospital_offset.world'
X='0.0'
Y='0.0'
Z='0.2'
yaw='0.0'

if [[ $# -gt 0 ]]
  then
  gazebo_world="$1"

  if [[ $# -eq 5 ]]
    then
    X="$2"
    Y="$3"
    Z="$4"
    yaw="$5"
  elif [[ $# -ne 1 ]]
    then
    echo "usage: run_sim.bash [gazebo world] [<X> <Y> <Z> <Yaw>]"
    exit 1
  fi
fi

session=$robot_name'_sim'

echo -e "\e[1m\e[4m\e[7mOpenning a tmux session named $session\e[0m"
tmux new -d -s $session

on_error() {
  echo -e "\e[1m\e[91m[$session]: an error occured, closing session.\e[0m"
  tmux kill-session -t $session
}
trap on_error ERR

sleep 1

window_num=1

echo -e "\e[1m\e[36m[$session-window1]: running 'ros2 launch $robot_name sim_bringup.launch.py odom_tf:=true world:=$gazebo_world'\e[0m"
tmux send -t $session "ros2 launch $robot_name sim_bringup.launch.py odom_tf:=true world:=$gazebo_world" C-m

sleep 7

echo -e "\e[1m\e[36m[$session-window$((window_num + 1))]: running 'ros2 launch $robot_name sim_toolbox_slam.launch.py'\e[0m"
tmux splitw -t $session
tmux send "ros2 launch $robot_name sim_toolbox_slam.launch.py" C-m
window_num=$((window_num + 1))

sleep 1

echo -e "\e[1m\e[36m[$session-window$((window_num + 1))]: running 'ros2 launch $robot_name navigation_sim.py'\e[0m"
tmux splitw -t $session

tmux send "ros2 launch $robot_name navigation_sim.py" C-m
window_num=$((window_num + 1))

tmux a -t $session
